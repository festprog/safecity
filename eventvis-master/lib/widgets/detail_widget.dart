import 'package:eventvis/models/event_model.dart';
import 'package:eventvis/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:maps_launcher/maps_launcher.dart';

class DetailWidget extends StatelessWidget {
  const DetailWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final List<dynamic> args = ModalRoute.of(context)?.settings.arguments as List<dynamic>;
    final EventModel event = args[0];
    Map<String, dynamic> eventData = _eventParser(event);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Detalle del evento."),
        centerTitle: true,
        backgroundColor: primaryColor,
      ),
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SingleChildScrollView(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox.fromSize(size: const Size(100, 15)),
              Center(
                child: FadeInImage(
                    width: 100,
                    height: 100,
                    placeholder: const AssetImage("assets/megaphone.png"),
                    image: AssetImage(eventData["image"]),
                    fit: BoxFit.scaleDown),
              ),
              SizedBox.fromSize(size: const Size(100, 15)),
              Row(
                children: const [
                  Text('Descripción:', style: formItemStyle),
                ],
              ),
              Column(children: [
                Text(
                  eventData["eventDescription"],
                  style: fieldItemStyle,
                  softWrap: true,
                ),
              ]),
              SizedBox.fromSize(size: const Size(100, 15)),
              Row(
                children: [
                  const Text('Fecha:', style: formItemStyle),
                  SizedBox.fromSize(size: const Size(5, 1)),
                  Text(eventData["date"], style: fieldItemStyle),
                  SizedBox.fromSize(size: const Size(15, 1)),
                  Row(
                    children: [
                      const Text('Hora:', style: formItemStyle),
                      SizedBox.fromSize(size: const Size(5, 1)),
                      Text(eventData["time"], style: fieldItemStyle),
                    ],
                  ),
                ],
              ),
              SizedBox.fromSize(size: const Size(100, 15)),
              Row(
                children: [
                  const Text('Lugar:', style: formItemStyle),
                  SizedBox.fromSize(size: const Size(5, 1)),
                  Text('${eventData["location"]}', style: fieldItemStyle),
                  SizedBox.fromSize(size: const Size(15, 1)),
                  Row(
                    children: [
                      const Text('Zona:', style: formItemStyle),
                      SizedBox.fromSize(size: const Size(5, 1)),
                      Text("${eventData["zoneCode"]}", style: fieldItemStyle),
                    ],
                  ),
                ],
              ),
              SizedBox.fromSize(size: const Size(100, 15)),
              Center(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(backgroundColor: primaryColor),
                  onPressed: () =>
                      MapsLauncher.launchCoordinates(eventData["location"][0], eventData["location"][1], 'Evento: ${eventData["eventDescription"]}'),
                  child: const Text('Ver en Maps'),
                ),
              ),
              SizedBox.fromSize(size: const Size(100, 15)),
              Row(
                children: [
                  const Text('Comentario:', style: formItemStyle),
                  SizedBox.fromSize(size: const Size(5, 1)),
                ],
              ),
              Column(
                children: [
                  Text(
                    eventData["comment"],
                    style: fieldItemStyle,
                    softWrap: true,
                  ),
                ],
              ),
            ],
          )),
        ),
      ),
    );
  }

  Map<String, dynamic> _eventParser(EventModel event) {
    Map<String, dynamic> eventData = event.toJson();

    if (event.eventDescription.isEmpty) {
      eventData["eventDescription"] = "Sin descripción.";
    }

    if (event.comment.isEmpty) {
      eventData["comment"] = "Sin comentario.";
    }

    switch (event.type) {
      case 1:
        {
          // Robo
          eventData["image"] = "assets/robbery.png";
          break;
        }
      case 2:
        {
          // Accidente
          eventData["image"] = "assets/collision.png";
          break;
        }
      case 3:
        {
          // Incendio
          eventData["image"] = "assets/fire.png";
          break;
        }
      default:
        {
          eventData["image"] = "assets/megaphone.png";
        }
    }
    return eventData;
  }
}
