import 'package:eventvis/models/event_model.dart';
import 'package:eventvis/services/event_services.dart';
import 'package:eventvis/theme/theme.dart';
import 'package:eventvis/user_preferences/user_preferences.dart';
import 'package:flutter/material.dart';

class EventsWidget extends StatelessWidget {
  const EventsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final prefs = UserPreferences();
    final EventServices eventServices = EventServices();

    return Scaffold(
        appBar: AppBar(
          title: const Text("Eventos"),
          centerTitle: true,
          backgroundColor: primaryColor,
        ),
        body: _createCardList(eventServices));
  }

  Map<String, dynamic> _eventParser(EventModel event) {
    Map<String, dynamic> eventData = event.toJson();

    if (event.eventDescription.isEmpty) {
      eventData["eventDescription"] = "Sin descripción.";
    }

    if (event.comment.isEmpty) {
      eventData["comment"] = "Sin comentario.";
    }

    switch (event.type) {
      case 1:
        {
          // Robo
          eventData["image"] = "assets/robbery.png";
          break;
        }
      case 2:
        {
          // Accidente
          eventData["image"] = "assets/collision.png";
          break;
        }
      case 3:
        {
          // Incendio
          eventData["image"] = "assets/fire.png";
          break;
        }
      default:
        {
          eventData["image"] = "assets/megaphone.png";
        }
    }

    return eventData;
  }

  _createCardList(EventServices eventServices) {
    debugPrint("Creando listados");

    return FutureBuilder(
        future: eventServices.getEvents(),
        builder: (BuildContext context, AsyncSnapshot<List<EventModel>> snapshot) {
          debugPrint("snap has data? >> ${snapshot.hasData} ");
          debugPrint(snapshot.toString());
          if (snapshot.hasData) {
            debugPrint("Creando tarjetas");
            final List<EventModel> events = snapshot.data!;

            return ListView.builder(
              itemCount: events.length,
              itemBuilder: (BuildContext context, int index) {
                var eventData = _eventParser(events[index]);
                var eventCard = Card(
                    elevation: 2,
                    borderOnForeground: true,
                    shadowColor: primaryColor,
                    color: bgColor,
                    clipBehavior: Clip.antiAlias,
                    child: InkWell(
                      splashColor: alternateColor,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        debugPrint("Evento #$index tocado.");
                        Navigator.pushNamed(context, "detail", arguments: [events[index]]);
                      },
                      child: Column(
                        children: [
                          FadeInImage(
                              width: 100,
                              height: 100,
                              placeholder: const AssetImage("assets/megaphone.png"),
                              image: AssetImage(eventData["image"]),
                              fit: BoxFit.scaleDown),
                          const SizedBox(width: 100, height: 10),
                          Text(eventData["eventDescription"], style: eventText),
                          const SizedBox(width: 100, height: 5),
                          Text("${eventData["date"]} ${eventData["time"]}", style: bodyText1),
                        ],
                      ),
                    ));
                return Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: eventCard,
                );
                // return Text(events[index].name);
              },
            );
          } else if (snapshot.hasError) {
            return const Icon(Icons.error_outline);
          } else {
            debugPrint("Cargando");
            return const Center(child: CircularProgressIndicator());
          }
        });
  }
}
