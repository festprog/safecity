import 'package:eventvis/services/access_services.dart';
import 'package:eventvis/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:otp_text_field/otp_text_field.dart';
import 'package:otp_text_field/style.dart';

class OtpWidget extends StatelessWidget {
  const OtpWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final TextEditingController codeController = TextEditingController();
    final AccessServices accessService = AccessServices();
    final List<String> args = ModalRoute.of(context)?.settings.arguments as List<String>;

    final userMail = args[0];
    final userName = args[1];

    var centralWidget = SafeArea(
        child: SingleChildScrollView(
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("Verificación de cuenta.",
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: primaryColor,
                )),
            const SizedBox(height: 20, width: double.infinity),
            Text("Ingresa el código que recibiste en $userMail",
                style: const TextStyle(
                  fontSize: 15,
                  color: Colors.black45,
                )),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: OTPTextField(
                length: 6,
                width: MediaQuery.of(context).size.width,
                fieldWidth: 50,
                style: const TextStyle(fontSize: 17),
                otpFieldStyle: OtpFieldStyle(
                  borderColor: primaryColor,
                ),
                textFieldAlignment: MainAxisAlignment.spaceAround,
                fieldStyle: FieldStyle.box,
                onChanged: (number) {
                  debugPrint("Número actual del pin: $number");
                },
                onCompleted: (code) {
                  debugPrint("Código ingresado: $code");

                  debugPrint("Enviando petición al servicio de verificación.");
                  final List<dynamic> result =
                      accessService.otp(username: userName, code: code, context: context) as List;

                  if (result[0] == true) {
                    Navigator.pushNamed(context, "login");
                  } else {}
                },
              ),
            ),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                child: SizedBox(
                  //width: double.infinity,
                  child: ElevatedButton(
                    onPressed: () {
                      debugPrint("Click botón de verificación.");
                      var code = codeController.text;

                      debugPrint("code: $code");

                      if (code.isNotEmpty && code.length >= 6) {
                        debugPrint("Enviando petición al servicio de verificación.");
                        final List<dynamic> result = accessService.otp(
                            username: userName, code: code, context: context) as List;
                        if (result[0] == true) {
                          Navigator.pushNamed(context, "login");
                        } else {}
                      } else {
                        debugPrint("No se envió la petición al servicio.");
                        debugPrint("Código muy corto.");
                        const snackBar = SnackBar(
                          content: Text('Código invalido.'),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      }
                    },
                    style: ElevatedButton.styleFrom(backgroundColor: primaryColor),
                    child: const Text("Verificar"),
                  ),
                )),
          ],
        ),
      ),
    ));
    return centralWidget;
  }
}
