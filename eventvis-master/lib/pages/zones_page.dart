import 'package:eventvis/services/zone_services.dart';
import 'package:eventvis/theme/theme.dart';
import 'package:flutter/material.dart';

class ZonesPage extends StatefulWidget {
  const ZonesPage({super.key});

  @override
  State<ZonesPage> createState() => _ZonesPageState();
}

class _ZonesPageState extends State<ZonesPage> {
  ZoneServices zonesServices = ZoneServices();
  late String zonas;

  @override
  void initState() {
    super.initState();
    zonas = "Lista de zonas";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Zonas"),
        centerTitle: true,
        backgroundColor: primaryColor,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: SizedBox(
              height: MediaQuery.of(context).size.height,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        getZones();
                      },
                      child: const Text("Cargar Zonas")),
                  const SizedBox(width: double.infinity, height: 20),
                  Text(
                    zonas,
                    softWrap: true,
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Future<void> getZones() async {
    zonas = await zonesServices.listZones();
    setState(() {});
  }
}
