import 'package:eventvis/widgets/detail_widget.dart';
import 'package:flutter/material.dart';

import '../theme/theme.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: DetailWidget(),
      backgroundColor: bgColor,
    );
  }
}
