import 'package:eventvis/user_preferences/user_preferences.dart';
import 'package:flutter/material.dart';

class FakeHomePage extends StatelessWidget {
  const FakeHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final prefs = UserPreferences();
    return Scaffold(
      appBar: AppBar(
        title: const Text("FakeHome"),
      ),
      body: Column(
        children: [
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "login");
              },
              child: const Text("a Log In")),
          SizedBox.fromSize(size: const Size(100, 15)),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "signin");
              },
              child: const Text("a Sign In")),
          SizedBox.fromSize(size: const Size(100, 15)),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "events");
              },
              child: const Text("a Eventos")),
          SizedBox.fromSize(size: const Size(100, 15)),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "main");
              },
              child: const Text("a Main")),
          SizedBox.fromSize(size: const Size(100, 15)),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "editprofile");
              },
              child: const Text("a Editar Usuario")),
          SizedBox.fromSize(size: const Size(100, 15)),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "changepass");
              },
              child: const Text("a Cambio contraseña")),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "location");
              },
              child: const Text("a Location")),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "zones");
              },
              child: const Text("a Zonas")),
          SizedBox.fromSize(size: const Size(100, 15)),
          ElevatedButton(
              onPressed: () {
                debugPrint("UserName = ${prefs.username}");
                debugPrint("Name = ${prefs.name}");
                debugPrint("LastName = ${prefs.lastname}");
                debugPrint("email = ${prefs.email}");
                debugPrint("token = ${prefs.token}");
              },
              child: Text("Depurar Prefs"))
        ],
      ),
    );
  }
}
