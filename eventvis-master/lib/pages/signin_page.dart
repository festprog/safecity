import 'package:eventvis/widgets/signin_widget.dart';
import 'package:flutter/material.dart';

class SigninPage extends StatelessWidget {
  const SigninPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
        body: Padding(
      padding: EdgeInsets.all(5.0),
      child: SingleChildScrollView(child: Center(child: SigninWidget())),
    ));
  }
}
