import 'package:eventvis/widgets/otp_pass_widget.dart';
import 'package:flutter/material.dart';

class OtpPassPage extends StatelessWidget {
  const OtpPassPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
        body: Padding(
      padding: EdgeInsets.all(5.0),
      child: SingleChildScrollView(child: Center(child: OtpPassWidget())),
    ));
  }
}
