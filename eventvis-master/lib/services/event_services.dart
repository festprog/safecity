import 'dart:convert';

import 'package:eventvis/models/event_model.dart';
import 'package:eventvis/user_preferences/user_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EventServices {
  final ip = "http://sistemic.udea.edu.co:4000";
  final prefs = UserPreferences();

  Future<List<EventModel>> getEvents() async {
    debugPrint("Solicitando eventos");

    var headers = {'Authorization': 'Bearer ${prefs.token}', 'Cookie': 'color=rojo'};

    var request = http.Request('GET', Uri.parse('$ip/reto/events/eventos/listar'));

    request.headers.addAll(headers);
    try {
      debugPrint("Construyendo solicitud");
      http.StreamedResponse response = await request.send().timeout(const Duration(seconds: 10));
      final decodedData = json.decode(await response.stream.bytesToString());
      debugPrint("Repuesta del servidor >> ${response.statusCode}");
      debugPrint("decodedData >> $decodedData");
      if (response.statusCode == 200) {
        debugPrint("Extrayendo eventos de la respuesta");
        final List<EventModel> events = [];
        decodedData.forEach((value) {
          events.add(EventModel.fromJson(value));
        });
        return events;
      } else {
        debugPrint("Fallo código ${response.statusCode}");
        debugPrint(response.reasonPhrase);
        return [];
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
      return [];
    }
  }
}
