import 'dart:convert';

import 'package:eventvis/user_preferences/user_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

// TODO: agregar contexto a los servicios para lanzar los mensajes de error o confirmacion

class UserServices {
  final ip = "http://sistemic.udea.edu.co:4000";
  final prefs = UserPreferences();

  Future getUserProfile() async {
    // Este método cargará los datos del perfil en las preferencias de usuario.
    debugPrint("Solicitando perfil del usuario");

    var headers = {'Authorization': 'Bearer ${prefs.token}', 'Cookie': 'color=rojo'};

    var request =
        http.Request('GET', Uri.parse('$ip/reto/usuarios/usuarios/encontrar/${prefs.username}'));

    request.headers.addAll(headers);

    try {
      debugPrint("Construyendo solicitud de perfil de usuario");
      http.StreamedResponse response = await request.send().timeout(const Duration(seconds: 10));
      final decodedData = json.decode(await response.stream.bytesToString());
      debugPrint("Respuesta >> ${response.statusCode}");
      debugPrint("decodedData >> $decodedData");

      if (response.statusCode == 200) {
        debugPrint("Extrayendo perfil de la respuesta");
        prefs.name = decodedData["name"];
        prefs.lastname = decodedData["lastName"];
        prefs.email = decodedData["email"];
      } else {
        debugPrint("Fallo código ${response.statusCode}");
        debugPrint(response.reasonPhrase);
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }

  Future editUser({required context, required String newName, required String newLastName}) async {
    debugPrint("Solicitando edición de usuario");
    var headers = {'Authorization': 'Bearer ${prefs.token}', 'Content-Type': 'application/json'};
    var request =
        http.Request('PUT', Uri.parse('$ip/reto/usuarios/usuarios/editar/${prefs.username}'));
    request.body =
        json.encode({"username": prefs.username, "name": newName, "lastName": newLastName});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send().timeout(const Duration(seconds: 10));

    debugPrint("Respuesta = ${response.statusCode}");
    if (response.statusCode == 200) {
      debugPrint(await response.stream.bytesToString());
    } else {
      final decodedData = json.decode(await response.stream.bytesToString());
      debugPrint("Error >> ${decodedData['message']}");

      final snackBar = SnackBar(
        content: Text("${decodedData['message']}"),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Future userExist() async {
    debugPrint("Solicitando existencia del usuario");

    var headers = {'Authorization': 'Bearer ${prefs.token}'};
    var request =
        http.Request('GET', Uri.parse('$ip/reto/usuarios/usuarios/existe/${prefs.username}'));

    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send().timeout(const Duration(seconds: 10));

    if (response.statusCode == 200) {
      // TODO: terminar esto
      print(await response.stream.bytesToString());
    } else {
      print(response.reasonPhrase);
    }
  }

  Future editPass({required String newPass, required String code, required context}) async {
    var result = [false];
    var headers = {'Authorization': 'Bearer ${prefs.token}'};
    var request = http.MultipartRequest(
        'PUT', Uri.parse('$ip/reto/usuarios/usuarios/editar-contrasena/${prefs.username}'));
    request.fields.addAll({'new-password': newPass, 'code': code});
    request.headers.addAll(headers);
    http.StreamedResponse response =
        await request.send().timeout(const Duration(milliseconds: 10000));

    debugPrint("Respuesta = ${response.statusCode}");
    if (response.statusCode == 200) {
      debugPrint(await response.stream.bytesToString());
      const snackBar = SnackBar(
        content: Text('Contraseña cambiada con éxito.'),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      result[0] = true;
    } else {
      final decodedData = json.decode(await response.stream.bytesToString());
      debugPrint("Error >> ${decodedData['message']}");
      final snackBar = SnackBar(
        content: Text("${decodedData['message']}"),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      result[0] = false;
    }
    return result;
  }

  Future editPassCode({
    required context,
  }) async {
    var headers = {'Authorization': 'Bearer ${prefs.token}'};
    var request = http.MultipartRequest(
        'PUT', Uri.parse('$ip/reto/usuarios/usuarios/codigo/${prefs.username}'));

    request.headers.addAll(headers);

    http.StreamedResponse response =
        await request.send().timeout(const Duration(milliseconds: 10000));

    debugPrint("Respuesta = ${response.statusCode}");
    if (response.statusCode == 200) {
      debugPrint(await response.stream.bytesToString());
      const snackBar = SnackBar(
        content: Text('Código enviado.'),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else {
      final decodedData = json.decode(await response.stream.bytesToString());
      debugPrint("Error >> ${decodedData['message']}");

      final snackBar = SnackBar(
        content: Text("${decodedData['message']}"),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}
