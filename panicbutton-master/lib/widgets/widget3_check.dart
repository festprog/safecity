import 'package:flutter/material.dart';
import 'package:v1botonpanico/services/services1_getinto.dart';
import 'package:v1botonpanico/theme/theme.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:v1botonpanico/user_preferences/user_preferences.dart';

class CodeWidget extends StatelessWidget {
  const CodeWidget({super.key});

  @override
  Widget build(BuildContext context) {
    // VARIABLES - INSTANCIAS
    final prefs = PreferencesUser();

    final ingresoServices = userServices();

    return SafeArea(
      child: SingleChildScrollView(
        child: SizedBox(
          // AJUSTA EL TAMAÑO DE LA PANTALLA
          height: MediaQuery.of(context).size.height,
          width: double.infinity,

          // REGISTRARSE EN LA APP
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Ingresa el código",
                style: TextStyle(
                    color: Color(0x9A031E | mask),
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
              ),
              Text("El código fue enviado al correo:" + prefs.email),
              const SizedBox(
                height: 40,
              ),

              // CÓDIGO
              OTPTextField(
                length: 6,
                width: MediaQuery.of(context).size.width,
                fieldWidth: 45,
                style: const TextStyle(fontSize: 17),
                textFieldAlignment: MainAxisAlignment.spaceEvenly,
                fieldStyle: FieldStyle.box,
                onCompleted: (pin) {
                  ingresoServices.code(pin, prefs.user, context);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
