import 'package:flutter/material.dart';
import 'package:v1botonpanico/services/services1_getinto.dart';
import 'package:v1botonpanico/theme/theme.dart';
import 'package:v1botonpanico/user_preferences/user_preferences.dart';
import 'form_widget.dart';

class LoginWidget extends StatelessWidget {
  LoginWidget({super.key});

  // VARIABLES - INSTANCIAS
  final TextEditingController userController = TextEditingController();
  final TextEditingController passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // VARIABLES - INSTANCIAS
    final prefs = PreferencesUser();
    debugPrint("PRIMERO: ${prefs.token}");
    final ingresoServices = userServices();

    return SafeArea(
      child: SingleChildScrollView(
        child: SizedBox(
          // AJUSTAR TAMAÑO PANTALLA
          height: MediaQuery.of(context).size.height,
          width: double.infinity,

          // INICIAR SESIÓN
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // IMAGEN LOGO FESTIVAL
              const Image(
                image: AssetImage("LogoFestival.png"),
                width: 300,
              ),

              // BOX USERNAME
              FormWidget(
                  label: "Nombre de usuario", controllerText: userController),

              // BOX PASSWORD
              FormWidget(
                label: "Contraseña",
                controllerText: passController,
                obscure: true,
              ),

              // RECUPERAR CONTRASEÑA
              /*Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, "changepass");
                      },
                      child: const Text(
                        "¿Haz olvidado tu contraseña?",
                        style: TextStyle(
                            color: textColor,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      )
                  ),
                ],
              ),*/

              // BOTÓN INICIAR SESIÓN
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: SizedBox(
                  width: 200,
                  height: 40,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color(0x0F4C5C | mask),
                      ),
                      onPressed: () {
                        ingresoServices.login(
                            userController.text, passController.text, context);
                        prefs.user = userController.text;
                        prefs.password = passController.text;
                      },
                      child: const Text("Iniciar sesión")),
                ),
              ),

              // CREAR CUENTA
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("¿No tienes cuenta?"),
                  TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, "sigin");
                      },
                      child: const Text(
                        "Crear cuenta",
                        style: TextStyle(
                            color: Color(0x5F0F40 | mask),
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      )),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
