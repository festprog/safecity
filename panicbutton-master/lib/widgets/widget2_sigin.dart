import 'package:flutter/material.dart';
import 'package:v1botonpanico/services/services1_getinto.dart';
import 'package:v1botonpanico/theme/theme.dart';
import 'form_widget.dart';
import 'package:v1botonpanico/user_preferences/user_preferences.dart';

class SiginWidget extends StatelessWidget {
  SiginWidget({super.key});

  final prefs = PreferencesUser();

  final TextEditingController userNameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // VARIABLES - INSTANCIAS
    final ingresoServices = userServices();

    return SafeArea(
      child: SingleChildScrollView(
        child: SizedBox(
          // AJUSTA EL TAMAÑO DE LA PANTALLA
          height: MediaQuery.of(context).size.height,
          width: double.infinity,

          // REGISTRARSE EN LA APP
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 40,
              ),

              // USERNAME
              FormWidget(
                label: "Nombre de usuario",
                controllerText: userNameController,
              ),

              // PASSWORD
              FormWidget(
                  label: "Contraseña",
                  controllerText: passwordController,
                  obscure: true),

              // CORREO
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  controller: emailController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Correo",
                  ),
                ),
              ),

              // BOTON DE SIGIN
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                child: SizedBox(
                  width: 200,
                  height: 40,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: const Color(0x0F4C5C | mask)),
                      onPressed: () {
                        ingresoServices.sigin(
                            userNameController.text,
                            passwordController.text,
                            emailController.text,
                            context);
                        prefs.email = emailController.text;
                        Navigator.pushNamed(context, "codigo");
                      },
                      child: const Text("Siguiente")),
                ),
              ),

              // VOLVER A PANTALLA INICIAR SESIÓN
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("¿Tienes una cuenta?"),
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text(
                        "Iniciar sesión",
                        style: TextStyle(
                            color: textColor,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      )),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
