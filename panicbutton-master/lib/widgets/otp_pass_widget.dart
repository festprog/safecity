import 'package:v1botonpanico/services/user_services.dart';
import 'package:v1botonpanico/theme/theme.dart';
import 'package:v1botonpanico/user_preferences/user_preferences.dart';
import 'package:flutter/material.dart';
import 'package:otp_text_field/otp_text_field.dart';
import 'package:otp_text_field/style.dart';

class OtpPassWidget extends StatelessWidget {
  const OtpPassWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final PreferencesUser prefs = PreferencesUser();
    final TextEditingController codeController = TextEditingController();
    final UserServices userService = UserServices();
    final List<dynamic> args = ModalRoute.of(context)?.settings.arguments as List<dynamic>;
    final newPass = "${args[0]}";

    var centralWidget = SafeArea(
        child: SingleChildScrollView(
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("Verificación de cambio de contraseña.", style: subtitleText1),
            const SizedBox(height: 20, width: double.infinity),
            Text("Ingresa el código que recibiste en ${prefs.email}",
                style: const TextStyle(
                  fontSize: 15,
                  color: Colors.black45,
                )),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: OTPTextField(
                length: 6,
                width: MediaQuery.of(context).size.width,
                fieldWidth: 50,
                style: const TextStyle(fontSize: 17),
                otpFieldStyle: OtpFieldStyle(
                  borderColor: primaryColor,
                ),
                textFieldAlignment: MainAxisAlignment.spaceAround,
                fieldStyle: FieldStyle.box,
                onCompleted: (code) async {
                  debugPrint("Código ingresado: $code");
                  final List<dynamic> result =
                      await userService.editPass(newPass: newPass, code: code, context: context);
                  if (result[0] == true) {
                    Navigator.pushNamed(context, "editprofile");
                  } else {}
                },
              ),
            ),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                child: SizedBox(
                  //width: double.infinity,
                  child: ElevatedButton(
                    onPressed: () async {
                      debugPrint("Click botón de verificación.");
                      var code = codeController.text;
                      debugPrint("code: $code");
                      if (code.isNotEmpty && code.length >= 6) {
                        debugPrint("Enviando petición al servicio de verificación.");
                        final List<dynamic> result = await userService.editPass(
                            code: code, context: context, newPass: newPass);
                        if (result[0] == true) {
                          Navigator.pushNamed(context, "editprofile");
                        } else {}
                      } else {
                        debugPrint("No se envió la petición al servicio.");
                        debugPrint("Código muy corto.");
                        const snackBar = SnackBar(
                          content: Text('Código invalido.'),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      }
                    },
                    style: ElevatedButton.styleFrom(backgroundColor: primaryColor),
                    child: const Text("Verificar"),
                  ),
                )),
          ],
        ),
      ),
    ));
    return centralWidget;
  }
}
