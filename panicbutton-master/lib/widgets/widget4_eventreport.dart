import 'package:flutter/material.dart';
import 'package:v1botonpanico/services/services_eventreport.dart';
import 'package:v1botonpanico/theme/theme.dart';
import '../user_preferences/user_preferences.dart';
import 'package:location/location.dart';

class EventReportWidget extends StatefulWidget {
  const EventReportWidget({super.key});

  @override
  State<EventReportWidget> createState() => _EventReportWidgetState();
}

class _EventReportWidgetState extends State<EventReportWidget> {
  // VARIABLES - INSTANCIAS
  final scaffoldKey = GlobalKey<ScaffoldState>();

  ReportServices rs = ReportServices();
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController commentController = TextEditingController();
  double lat = 0.0;
  double lng = 0.0;
  String event = "";
  final prefs = PreferencesUser();
  final TextEditingController locationController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // VARIABLES - INSTANCIAS
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: const Color(0xFFFDFDFD | mask),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(0, 50, 0, 0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // BOTON PARA EDITAR PERFIL
                Align(
                  alignment: const AlignmentDirectional(0, 0),
                  child: Padding(
                      padding:
                          const EdgeInsetsDirectional.fromSTEB(500, 0, 0, 20),
                      child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ElevatedButton(
                                onPressed: () async {
                                  Navigator.pushNamed(context, "editprofile");
                                },
                                child: const Text('Perfil'),
                                style: ElevatedButton.styleFrom(
                                    backgroundColor:
                                        const Color(0x5F0F40 | mask))),
                          ])),
                ),

                //
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(20, 0, 20, 0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsetsDirectional.fromSTEB(0, 16, 0, 0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsetsDirectional.fromSTEB(
                                    0, 16, 0, 0),
                                child: TextFormField(
                                  controller: descriptionController,
                                  obscureText: false,
                                  decoration: InputDecoration(
                                    //labelText: 'Título descripció',
                                    hintText: 'Descripción',
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                        color: Color(0xE36414 | mask),
                                        width: 2,
                                      ),
                                      borderRadius: BorderRadius.circular(40),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                        ///// AQUI REVISAR
                                        color: Color(0xFB8B24 | mask),
                                        width: 2,
                                      ),
                                      borderRadius: BorderRadius.circular(40),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                        color: Color(0xFFFFDFDFD | mask),
                                        width: 2,
                                      ),
                                      borderRadius: BorderRadius.circular(40),
                                    ),
                                    focusedErrorBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                        color: Color(0xFFFFDFDFD | mask),
                                        width: 2,
                                      ),
                                      borderRadius: BorderRadius.circular(40),
                                    ),
                                    filled: true,
                                    fillColor: const Color(0xFFFFDFDFD | mask),
                                    contentPadding:
                                        const EdgeInsetsDirectional.fromSTEB(
                                            16, 24, 0, 24),
                                  ),
                                  style: bodyText1,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsetsDirectional.fromSTEB(0, 16, 0, 0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: TextFormField(
                                controller: commentController,
                                obscureText: false,
                                decoration: InputDecoration(
                                  labelStyle: bodyText2,
                                  hintText: 'Comentario',
                                  hintStyle: bodyText2,
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Color(0xE36414 | mask),
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.circular(40),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Color(0xFB8B24 | mask),
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.circular(40),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Color(0x00000000),
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.circular(40),
                                  ),
                                  focusedErrorBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Color(0x00000000),
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.circular(40),
                                  ),
                                  filled: true,
                                  fillColor: Color(0xFFFFDFDFD | mask),
                                  contentPadding:
                                      const EdgeInsetsDirectional.fromSTEB(
                                          16, 24, 24, 24),
                                ),
                                style: bodyText1,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsetsDirectional.fromSTEB(0, 24, 0, 0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Latitud=$lat  Longitud=$lng",
                              style: subtitleText2,
                            ),
                            Align(
                              alignment: const AlignmentDirectional(-0.05, 0),
                              child: ElevatedButton(
                                  onPressed: () async {
                                    final List<dynamic> result =
                                        await getLocation();
                                    bool isOk = result[0];
                                    if (isOk) {
                                      lat = result[1];
                                      lng = result[2];
                                      setState(() {});
                                    }
                                  },
                                  child: const Text('Ubicación',
                                      style: buttonText2),
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor:
                                          const Color(0x0F4C5C | mask))),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsetsDirectional.fromSTEB(0, 24, 0, 12),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Text(
                              'Selecciona el tipo de evento',
                              textAlign: TextAlign.center,
                              style: subtitleText1,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            InkWell(
                              onTap: () async {
                                setState(() {
                                  event = "Hurto";
                                });
                              },
                              child: Container(
                                width: 50,
                                height: 50,
                                decoration: const BoxDecoration(
                                  color: Color(0xFFFDFDFD | mask),
                                  boxShadow: [
                                    BoxShadow(
                                      blurRadius: 5,
                                      color: Color(0x3314181B),
                                      offset: Offset(0, 2),
                                    )
                                  ],
                                  shape: BoxShape.circle,
                                ),
                                alignment: const AlignmentDirectional(0, 0),
                                child: Image.asset(
                                  'assets/robbery.png',
                                  width: 35,
                                  height: 35,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () async {
                                setState(() {
                                  event = "Accidente automovilístico";
                                });
                              },
                              child: Container(
                                width: 50,
                                height: 50,
                                decoration: const BoxDecoration(
                                  color: Color(0xFFFDFDFD | mask),
                                  boxShadow: [
                                    BoxShadow(
                                      blurRadius: 5,
                                      color: Color(0x3314181B),
                                      offset: Offset(0, 2),
                                    )
                                  ],
                                  shape: BoxShape.circle,
                                ),
                                alignment: const AlignmentDirectional(0, 0),
                                child: Image.asset(
                                  'assets/collision.png',
                                  width: 35,
                                  height: 35,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () async {
                                setState(() {
                                  event = "Incendio";
                                });
                              },
                              child: Container(
                                width: 50,
                                height: 50,
                                decoration: const BoxDecoration(
                                  color: Color(0xFFFDFDFD | mask),
                                  boxShadow: [
                                    BoxShadow(
                                      blurRadius: 5,
                                      color: Color(0x3314181B),
                                      offset: Offset(0, 2),
                                    )
                                  ],
                                  shape: BoxShape.circle,
                                ),
                                alignment: const AlignmentDirectional(0, 0),
                                child: Image.asset(
                                  'assets/fire.png',
                                  width: 35,
                                  height: 35,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () async {
                                setState(() {
                                  event = "Otro";
                                });
                              },
                              child: Container(
                                width: 50,
                                height: 50,
                                decoration: const BoxDecoration(
                                  color: Color(0xFFFDFDFD | mask),
                                  boxShadow: [
                                    BoxShadow(
                                      blurRadius: 5,
                                      color: Color(0x3314181B),
                                      offset: Offset(0, 2),
                                    )
                                  ],
                                  shape: BoxShape.circle,
                                ),
                                alignment: const AlignmentDirectional(0, 0),
                                child: Image.asset(
                                  'assets/megaphone.png',
                                  width: 35,
                                  height: 35,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsetsDirectional.fromSTEB(0, 24, 0, 12),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Tipo de evento: $event",
                              textAlign: TextAlign.center,
                              style: subtitleText2,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: const AlignmentDirectional(-0.05, 0),
                  child: Padding(
                    padding: const EdgeInsetsDirectional.fromSTEB(0, 20, 0, 0),
                    child: ElevatedButton(
                        onPressed: () async {
                          debugPrint(descriptionController.text);
                          debugPrint(commentController.text);
                          List<double> location = [lat, lng];
                          debugPrint(location.toString());
                          rs.report(location, descriptionController.text,
                              commentController.text, prefs.user, context);
                        },
                        child: const Text('Enviar'),
                        style: ElevatedButton.styleFrom(
                            backgroundColor: const Color(0x0F4C5C | mask))),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<List> getLocation() async {
    Location location = Location();

    bool serviceEnabled;
    PermissionStatus permissionGranted;
    LocationData locationData;

    serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return [false];
      }
    }

    permissionGranted = await location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return [false];
      }
    }

    locationData = await location.getLocation();
    final double? lat = locationData.latitude;
    final double? lng = locationData.longitude;
    return [true, lat, lng];
  }
}
