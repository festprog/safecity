import 'package:v1botonpanico/user_preferences/user_preferences.dart';
import 'package:flutter/material.dart';

class FakeHomePage extends StatelessWidget {
  const FakeHomePage({super.key});

  /*ESTA PÁGINA SIRVE PARA VIAJAR A LAS PANTALLAS DE FORMA FÁCIL*/

  @override
  Widget build(BuildContext context) {
    final prefs = PreferencesUser();
    return Scaffold(
      appBar: AppBar(
        title: const Text("FakeHome"),
      ),
      body: Column(
        children: [
          SizedBox.fromSize(size: const Size(100, 15)),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "login");
              },
              child: const Text("a Log In")),
          SizedBox.fromSize(size: const Size(100, 15)),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "editprofile");
              },
              child: const Text("a Editar Usuario")),
          SizedBox.fromSize(size: const Size(100, 15)),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "changepass");
              },
              child: const Text("a Cambio contraseña")),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "location");
              },
              child: const Text("a Location")),
          ElevatedButton(
              onPressed: () {
                debugPrint("UserName = ${prefs.user}");
                debugPrint("Name = ${prefs.name}");
                debugPrint("LastName = ${prefs.lastname}");
                debugPrint("email = ${prefs.email}");
                debugPrint("token = ${prefs.token}");
              },
              child: Text("Depurar Prefs"))
        ],
      ),
    );
  }
}
