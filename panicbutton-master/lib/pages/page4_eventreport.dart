import 'package:flutter/material.dart';
import 'package:v1botonpanico/theme/theme.dart';
import 'package:v1botonpanico/widgets/widget4_eventreport.dart';
import 'package:v1botonpanico/widgets/widget3_check.dart';
import 'package:v1botonpanico/services/services1_getinto.dart';

class eventreportPage extends StatefulWidget {
  const eventreportPage({super.key});

  @override
  State<eventreportPage> createState() => _eventreportPageState();
}

class _eventreportPageState extends State<eventreportPage> {
  final userServices _user = userServices();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFDFDFD),
      // APPBAR ES EL TITULO Y FRANJA AZUL DE ARRIBA
      appBar: AppBar(
        title: const Text('Reportar un evento'),
        centerTitle: true,
        backgroundColor: const Color(0x0F4C5C | mask),
        actions: <Widget>[
          IconButton(
            //atras
            iconSize: 40.0,
            onPressed: () async {
              await _user.logOut();

              Navigator.of(context).pushNamedAndRemoveUntil(
                  'login', (Route<dynamic> route) => false);
            },
            icon: const Icon(Icons.logout_rounded),
            color: Colors.white,
          ),
        ],
      ),
      body: EventReportWidget(),
    );
  }
}
