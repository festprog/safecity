import 'package:flutter/material.dart';
import 'package:v1botonpanico/theme/theme.dart';
import 'package:v1botonpanico/widgets/widget2_sigin.dart';

class SiginPage extends StatelessWidget {
  const SiginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFDFDFD),
      // APPBAR ES EL TITULO Y FRANJA AZUL DE ARRIBA
      appBar: AppBar(
        title: const Text('Registrarse'),
        centerTitle: true,
        backgroundColor: const Color(0x0F4C5C | mask),
      ),
      body: SiginWidget(),
    );
  }
}
