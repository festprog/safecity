import 'package:v1botonpanico/theme/theme.dart';
import 'package:v1botonpanico/widgets/edit_profile_widget.dart';
import 'package:flutter/material.dart';

class EditProfilePage extends StatelessWidget {
  const EditProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFDFDFD),
      appBar: AppBar(
        // TITULO BARRA SUPERIOR
        title: const Text('Perfil'),
        centerTitle: true,
        backgroundColor: const Color(0x0F4C5C | mask),
        elevation: 0,
      ),
      body: Padding(
        padding: EdgeInsets.all(0),
        child: Center(child: EditProfileWidget()),
      ),
    );
  }
}
