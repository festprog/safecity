import 'dart:convert';
import 'dart:html';
import 'package:v1botonpanico/user_preferences/user_preferences.dart';
import "package:http/http.dart" as http;
import 'package:flutter/material.dart';

class userServices {
  // VARIABLES - INSTANCIAS
  final prefs = PreferencesUser();
  final ip = "http://sistemic.udea.edu.co:4000/reto";

  // FUNCIONES PARA INICIO SESIÓN
  // La función recibe el usuario y password
  Future login(String usuario, String password, BuildContext context) async {
    List result = [false];
    // NO TOCAR
    var headers = {
      'Authorization': 'Basic Zmx1dHRlci1yZXRvOnVkZWE=',
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    // ENLACE A LA API
    var request =
        http.Request('POST', Uri.parse('$ip/autenticacion/oauth/token'));
    request.bodyFields = {
      'username': usuario,
      'password': password,
      'grant_type': 'password'
    };
    request.headers.addAll(headers);

    try {
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        final Map<String, dynamic> decodedData =
            json.decode(await response.stream.bytesToString());
        prefs.user = usuario;
        prefs.token = decodedData["access_token"];
        prefs.password = password;
        //userServices.getUserProfile();
        result[0] = true;
        debugPrint(prefs.token);
        Navigator.pushNamed(context, "eventReport");
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return const AlertDialog(
                title: Text('Intentar de nuevo'),
                content: Text('Contraseña o usuario incorrectos'),
                contentPadding: EdgeInsets.all(20),
              );
            });
        debugPrint(response.reasonPhrase);
        result[0] = false;
      }
    } catch (e) {
      // Ventana emergente que diga error de conexion al servidor
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return const AlertDialog(
              title: Text('Intentar de nuevo'),
              content: Text('Error de conexión'),
              contentPadding: EdgeInsets.all(20),
            );
          });
      debugPrint(e.toString());
    }
  }

  // FUNCIONES PARA REGISTRO
  // String name, String lastname, String email, String phone, String username, String password
  Future sigin(String username, String password, String email,
      BuildContext context) async {
    List result = [false];

    //NO MODIFICAR
    var headers = {'Content-Type': 'application/json'};
    var request =
        http.Request('POST', Uri.parse('$ip/usuarios/registro/enviar'));
    request.body = json
        .encode({"username": username, "password": password, "email": email});
    request.headers.addAll(headers);

    try {
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        prefs.user = username;
        prefs.password = password;
        prefs.email = email;
        result[0] = true;

        debugPrint(await response.stream.bytesToString());
      } else {
        print(response.reasonPhrase);
        result[0] = false;
      }
    } catch (e) {
      print(e);
    }
  }

  // VERIFICACIÓN CÓDIGO
  Future code(String code, String username, BuildContext context) async {
    List result = [false];

    var headers = {'Cookie': 'color=rojo; color=rojo'};
    var request = http.MultipartRequest(
        'POST', Uri.parse('$ip/usuarios/registro/confirmar/$username'));
    request.fields.addAll({'codigo': code});

    request.headers.addAll(headers);

    try {
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        final Map<String, dynamic> decodeData =
            json.decode(await response.stream.bytesToString());
        result[0] = true;
        debugPrint(await response.stream.bytesToString());
        Navigator.pushNamed(context, "login", arguments: [prefs.user]);
      } else {
        debugPrint(response.reasonPhrase);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return const AlertDialog(
              title: Text("Verificar código"),
              content: Text("Código incorrecto, intenta de nuevo"),
              contentPadding: EdgeInsets.all(20),
            );
          },
        );
        debugPrint(response.reasonPhrase);
        result[0] = false;
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future logOut() async {
    try {
      prefs.user = "";
      prefs.password = "";
      prefs.token = "";
    } catch (e) {
      //return null;
    }
  }
}
